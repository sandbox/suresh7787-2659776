<?php

/**
 * @file
 * Helps to create a checkout pane for donation line item.
 */

/**
 * Implements base_settings_form().
 */
function commerce_donation_checkout_pane_pane_settings_form($checkout_pane) {

  $form['commerce_donation_checkout_pane_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Donation Title'),
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_donation_checkout_pane_title', 'Donate'),
    '#description' => t('Enter the donation title which will appear in the checkout form'),
  );

  $form['commerce_donation_checkout_pane_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Donation Amount'),
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_donation_checkout_pane_amount', 1),
    '#field_prefix' => commerce_default_currency(),
    '#element_validate' => array('commerce_donation_checkout_pane_amount_validate'),
  );

  $form['commerce_donation_checkout_pane_ajaxify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Ajax for Commerce Donation in Checkout'),
    '#title_display' => 'after',
    '#description' => t('Please note that, Enabling ajax will add the donation line item to the cart order and will not refresh the order summary. If you want to do it please refer hook_ajax_commands_render_alter()'),
    '#default_value' => variable_get('commerce_donation_checkout_pane_ajaxify', FALSE),
  );

  return $form;
}

/**
 * Function to validate commerce donation form validate.
 */
function commerce_donation_checkout_pane_amount_validate($form, &$form_state) {
  if ($form_state['values']['commerce_donation_checkout_pane_amount'] <= 0) {
    form_set_error('commerce_donation_checkout_pane_amount', t('Please enter valid amount.'));
  }
}

/**
 * Implements base_checkout_form().
 */
function commerce_donation_checkout_pane_pane_checkout_form($form, $form_state) {
  global $user;

  $item_price = variable_get('commerce_donation_checkout_pane_amount', 1) * 100;
  $donation_title = variable_get('commerce_donation_checkout_pane_title', 'Donate');
  $title = t('@title @donation', array(
    '@title' => $donation_title,
    '@donation' => commerce_currency_format($item_price, commerce_default_currency()),
      )
  );

  $donation_line_item = FALSE;
  // Get the order object from from_state instead of loading.
  if (isset($form_state['order']->order_number)) {
    $order_wrapper = entity_metadata_wrapper("commerce_order", $form_state['order']);
    foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
      if ($line_item_wrapper->type->value() == 'commerce_donation_checkout_pane') {
        $donation_line_item = TRUE;
      }
    }
  }
  $checkout_form['commerce_donation_checkout_pane'] = array(
    '#type' => 'checkbox',
    '#title' => $title,
    '#title_display' => 'after',
    '#prefix' => '<div id=commerce-donation>',
    '#suffix' => '</div>',
    '#default_value' => (!empty($donation_line_item)) ? 1 : 0,
  );

  // Enable the Ajax for the donation checkbox in checkout.
  if (variable_get('commerce_donation_checkout_pane_ajaxify', FALSE)) {
    $checkout_form['commerce_donation_checkout_pane']['#ajax'] = array(
      'callback' => 'commerce_donation_checkout_pane_pane_checkout_form_submit',
      'wrapper' => 'commerce-donation',
      'method' => 'replace',
    );
  }
  return $checkout_form;
}

/**
 * Implements the base_checkout_form_submit().
 */
function commerce_donation_checkout_pane_pane_checkout_form_submit($form, &$form_state) {
  $commands = array();
  $commands[] = ajax_command_remove('.messages');
  $donation_amount = variable_get('commerce_donation_checkout_pane_amount', 1);  
  if (isset($form_state['order']->order_number)) {
    $form_order = $form_state['order'];
    $order = commerce_order_load($form_order->order_number);
    $order_wrapper = entity_metadata_wrapper("commerce_order", $order);
  }
  switch ($form_state['values']['donation']['commerce_donation_checkout_pane']) {
    case 0:
      commerce_donation_checkout_pane_delete_line_item($order_wrapper);
      drupal_set_message(t("Your donation has been removed."), 'status');
      break;
    case 1:
      $line_item = commerce_donation_checkout_pane_line_item_create($donation_amount, $order);
      commerce_donation_checkout_pane_add_line_item($line_item, $order);
      drupal_set_message(t("Your donation has been added."), 'status');
      break;
  }
  // Return the result as ajax commands if ajax enabled.
  $commands[] = ajax_command_prepend('#commerce-donation', theme('status_messages'));
  if (variable_get('commerce_donation_checkout_pane_ajaxify', FALSE)) {
    return array('#type' => 'ajax', '#commands' => $commands);
  }
}
