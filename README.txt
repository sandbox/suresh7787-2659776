ONTENTS OF THIS FILE
----------------------
  * Introduction
  * Requirements
  * Installation
  * Configure

INTRODUCTION
-------------
This module provides a checkbox at the checkout page to add 
the donation to the order. 

REQUIREMENTS
------------

1. Commerce module and Commerce checkout module is required for this module.

INSTALLATION
------------

Download this module and put it under sites/all/modules. 
Go to module administration page and enable this module.

CONFIGURE
----------

For this module, Donation title and Donation amount is configurable. 
Those configurations can be done in the 
following link : admin/commerce/config/checkout/form/pane/donation
